class App {
    //declare variables
    String? name ;
    String? sector ;
    String? developer ;
    int? year ;
 
    void printAppInfo()
    {
        name = name?.toUpperCase();
        var year1 = year?.toString();
        print("App name: $name");
        print("App Sector/Category: $sector");
        print("App Developer: $developer");
        print("App Year: $year1");
    }
}
 
void main()
{
    App app = new App();
 
    app.name = 'mtnApp Academy';
    app.sector = 'Learning Platform';
    app.developer = 'Oratile';
    app.year = 2022;
 
    app.printAppInfo();
}
